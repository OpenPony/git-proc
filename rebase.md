Rebaser permet de remettre une branche "tirée" il y a un moment au niveau actuel du projet

### Commande de rebasage
git pull --rebase (équivaut à faire git fetch && git rebase) ou git rebase

Si le projet n'a pas de conflit, le rebasage se fera d'une traite.

En cas de conflits, ils seront indiqués et votre branche sera dans un "état particuliers"

Il faut les résoudre :
  git status donnera les infos des fichiers en conflit
  résoudre les conflits
  ajouter les fichiers avec git add
  puis git commit

puis lancer la commande git rebase --continue

Si les modifications proposées ne conviennent pas : git rebase --skip
Pour annuler la gestion des conflits lors du rebasage : git rebase --abort

Pousser ensuite les modifications.
Il est possible de devoir forcer la pousser avec : git push -f origin <branche>

### Gérer les problèmes de rebasage

- Vous avez un problème pour rebaser à cause d'un commit en attente :
    Mettre de côté les commit : git stash
    Rebasage : git pull --rebase origin master
    Appliquer à nouveau les commit : git stash apply

### Mode interactif : permet de suivre toutes les étapes du rebasage et lancer les commandes à chaque étape
- Liste des commits en attente
- les actions peuvent être changées
    * pick par défaut
    * edit : utilise le commit mais action en cours stoppée pour l'amender
    * squash : combine le commit avec le précédent pour en créer un nouveau

```
git add -i
```

  Différents choix disponibles :
    * 1 : status
    * 2 : mise à jour
    * 3 : revert - retire des fichiers de l'index
    * 4 : add untracked - ajoute des fichiers non suivis
    * 5 : patch - assemblage de patchs (avec possibilité de choisir ou non les patchs à assembler)
    * 6 : view diff - voir les différences
    * 7 : quit
git commit : lance le commit pour enregistrer les modifs

git commit -a : annule toutes les modifications

### Réécrire l'historique
Vous avez fait pleins de petits commits sur une branche et avant de fusionner, vous aimeriez remettre tout ça au propre avant de merger une branche dans master ? squash est pour vous !

Il s'agit en fait d'une commande de rebasage qui permet de réécrire l'historique dans un seul commit.

Concrètement :

- On récupère le commit dans lequel on veut remettre tous les suivants et on lance la commande de rebasage :

```
git rebase -i <hash du commit>
```

- Le mode intéractif apparaît avec la liste des commits à partir du hash que vous avez choisi. Les hashs de commits sont précédés de la mention "pick". Il vous suffit alors de laisser "pick" sur le premier commit et de mettre "squash" pour tous les suivants. Vous basez ainsi sur le premier commit et vous lui appliquez les suivants.

- Validez le squash
- Indiquez un message à ce commit
- Poussez votre modification en la forçant (--force ou -f)
