### créer une branche
```
git checkout -b {branche}
```

### Avoir la liste des branches
```
git branch
```
* indique la branche sur laquelle nous sommes

### naviguer dans les branches
```
git checkout {branche}
```

ex : git checkout master => retour dans la branche master

### supprimer une branche
```
git branch -d {branche}
```

### pousser une branche locale sur le repo distant
```
git push origin {branche}
```

### voir différences entre branches :
```
git diff {branche source} {branche de destination}
```
