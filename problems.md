### Vous avez commité dans la mauvaise branche ou oublié de créer une branche avant de commit
  récupérer le n° du commit : git log

  créer branche correcte ou aller sur branche

  git cherry-pick {n°commit}

### Vous avez fait des "bêtises" sur un fichier et vous souhaitez récupérer ce fichier sur la dernière position connue de HEAD
```
git checkout -- {filename}
```

### Vous avez fait un commit dans master au lieu de créer une branche et vous ne savez pas comment remettre master dans son état initial

vous annulez le commit mais vous ne perdez pas vos modifs :
```
git reset HEAD~1
```

vous annulez le commit et vous voulez annuler aussi vos modifs :
```
git reset HEAD~1 --hard
```
