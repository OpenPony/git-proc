### fusionner branche
Se positionner sur la branche à conserver

```
git merge {branch}
```

{branch} sera alors mergé dans la branche où vous êtes.

--no-commit : ne fera pas le commit de merge qu'il faudra faire à la main => permet de vérifier avant le commit de merge

### conflit de merge (aka "conflit de merde")
En cas de conflits, ils seront indiqués et votre branche sera dans un "état particulier"

Il faut les résoudre :

  git status donnera les infos des fichiers en conflit

  résoudre les conflits

  ajouter les fichiers avec git add

  puis git commit

### Visualiser conflits éventuels
  git merge-base {branch 1} {branch 2} : permet de retrouver l'état initial de la base

  git describe --all -always $(git merge {branch 1} {branch 2})


  git show:1:(fichier) => permet de voir le contenu de base

  idem : 2 => voir le contenu local

  idem : 3 => voir le contenu distant


  git mergetool pour résoudre les conflits

  git checkout -m run.sh : permet de revenir en arrière de la résolution de conflit et remettre les marqueurs de conflits


### Pour annuler un merge en cas de blocage et revenir à l'état initial
```
git reset --hard HEAD
```

### Pour annuler un merge déjà commité
```
git reset --hard ORIG_HEAD
```

### Ne pas faire le commit du merge
```
git merge --no-commit {branch}
```

### Merge Fast-Forward
quand ours == base (pas de modifs concurrentes)
Possibilité de lui demander de ne pas faire de FF pour garder historique : --no-ff

### Revert de merge

Il arrive de merger un branche et de s'apercevoir que les modifications entrainent une regression ou que ça ne fonctionne pas.
Dans ce cas, il faut annuler le merge :

```
git revert -m1 <hash du commit à annuler>
```
L'option -m1 indique qu'il faut revenir à 1 commit du n°de commit à annuler.
