## Configuration
```
git config
```

--global : option qui permet d'appliquer la configuation à l'ensemble des projets d'un utilisateur.
Pour appliquer des options spécifiques à un projet, il convient de ne pas utiliser l'option "global" et se placer dans le dossier du projet.

--system : option qui applique la commande à l'ensemble du système en écrivant directement dans le fichier /etc/gitconfig

--local : option qui applique la commande uniquement sur le projet

En cas de configurations multiples, ce sera le fichier gitconfig du dépôt qui l'emportera sur le fichier principal.

### Ignorer fichier .orig (peut être tout autre) qu'en local pour notre user
```
echo '*.orig' >> .git/info/exclude
```

### Avoir un identifiant et une adresse e-mail différent
```
git config user.name "utilisateur"
```
```
git config user.email mail@votre-super-mail.com
```

### Changer d'éditeur
```
git config core.editor "votre super éditeur"
```

Remplacer "votre super éditeur" par emacs, vim, nano...

### Modifier le message de commit

Vous pouvez personnaliser le message de commit qui ressemble initialement à :

  ligne de sujet

  description

  [ticket: X]

Créer un fichier avec le nouveau modèle de commit (ex : .gitmessage.txt)

Puis

```
git config commit.template $<chemin vers le fichier que vous venez créer>/.gitmessage.txt
```

Pour vérifier, taper git commit, l'éditeur doit s'ouvrir en faisant apparaître le message que vous venez de paramètrer.

### Outil de visualisation de différence de fusion

Outil de résolution externe (mergetool): kdiff3, vimdiff, p4merge, emerge... (possibilité de créer son propre outil)

```
git config merge.tool outil_choisi
```
