### Créer un nouveau projet
```
git init
```

### Récupérer un projet
```
git clone <projet>
```

### Mettre un projet local sur un serveur distant
```
git remote add origin {server}
```

### Récupérer les modifications distantes sur le dossier local
```
git pull origin {branche}
```

ex : git pull origin master => récupère les modifications de master

### Supprimer toutes les modifications locales et récupérer les données du serveur & faire pointer la branche principale locale dessus
```
git fetch origin
```
```
git reset --hard origin/master
```
### Savoir le statuts dans l'avancement du travail
```
git status
```
### Ajouter un fichier, enregistrer les modifications
```
git add {fichier}
```

Valider toutes les modifs : git add *

### Supprimer un fichier
```
git rm {fichier}
```

### Vérifier ce qui va être commiter
```
git diff --cached
```

### Faire un commit : cela prendra toutes les modifications sur le projet
```
git commit -m "ajouter description du contenu du commit"
```

### voir les différences
```
git add -p
```

### logs des commits
```
git log
```
git log -p : permet de voir les patchs

git log --pretty=oneline : log sur une ligne

git log --pretty=short : logs au format "short"

git log --pretty=format:'%h was %an, %ar, message: %s'

git log --pretty=format:'%h : %s' --graph

git log --pretty=format:'%h : %s' --topo-order --graph

git log --pretty=format:'%h : %s' --date-order --graph

### tagguer les commits
```
git tag {tag}
```

### interface graphique
gitk

### Stash
git stash pop : met de côté les modifs

git stash list : liste des modifs de côté

git stash apply : applique les modifs

git stash --include-untracked : add tous les fichiers non trackés dans un 3e commit d'attente

### Voir commit courant
```
git show
```
